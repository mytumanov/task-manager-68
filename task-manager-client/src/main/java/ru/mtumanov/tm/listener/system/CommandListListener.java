package ru.mtumanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.listener.AbstractListener;

@Component
public class CommandListListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String getArgument() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show command list";
    }

    @Override
    @NotNull
    public String getName() {
        return "commands";
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[Commands]");
        for (@Nullable final AbstractListener abstractListener : listeners) {
            if (abstractListener == null) continue;
            @Nullable final String name = abstractListener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}

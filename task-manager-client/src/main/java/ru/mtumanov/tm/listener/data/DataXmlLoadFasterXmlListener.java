package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataXmlLoadFasterXmlRq;
import ru.mtumanov.tm.dto.response.data.DataXmlLoadFasterXmlRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from xml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-xml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataXmlLoadFasterXmlRs response = getDomainEndpoint().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}

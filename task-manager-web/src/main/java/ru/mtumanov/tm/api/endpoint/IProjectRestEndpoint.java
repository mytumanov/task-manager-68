package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    @NotNull
    @WebMethod
    @GetMapping
    Collection<ProjectDTO> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/{id}")
    ProjectDTO findById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id);

    @NotNull
    @WebMethod
    @PostMapping
    ProjectDTO create(
            @WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project);

    @NotNull
    @WebMethod
    @PutMapping
    ProjectDTO update(
            @WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project);

    @WebMethod
    @DeleteMapping
    void delete(@WebParam(name = "project", partName = "project") @RequestBody @Nullable final ProjectDTO project);

    @WebMethod
    @DeleteMapping
    void deleteById(@WebParam(name = "id", partName = "id") @PathVariable("id") @Nullable final String id);

}

package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.ITaskRestEndpoint;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @GetMapping
    public Collection<TaskDTO> findAll() {
        List<TaskDTO> projects = taskService.findAll();
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @GetMapping("/{id}")
    public TaskDTO findById(@PathVariable("id") @Nullable final String id) {
        return taskService.findById(id);
    }

    @Override
    @NotNull
    @PostMapping
    public TaskDTO create(@RequestBody @Nullable final TaskDTO task) {
        return taskService.add(task);
    }

    @Override
    @DeleteMapping
    public void delete(@RequestBody @Nullable final TaskDTO task) {
        taskService.remove(task);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") @Nullable final String id) {
        taskService.removeById(id);
    }

    @Override
    @NotNull
    @PutMapping
    public TaskDTO update(@RequestBody @Nullable final TaskDTO task) {
        return taskService.update(task);
    }
}

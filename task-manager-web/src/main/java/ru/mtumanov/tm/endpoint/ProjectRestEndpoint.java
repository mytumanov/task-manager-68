package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.IProjectRestEndpoint;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @NotNull
    @GetMapping
    public Collection<ProjectDTO> findAll() {
        List<ProjectDTO> projects = projectService.findAll();
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @GetMapping("/{id}")
    public ProjectDTO findById(@PathVariable("id") @Nullable final String id) {
        return projectService.findById(id);
    }

    @Override
    @NotNull
    @PostMapping
    public ProjectDTO create(@RequestBody @Nullable final ProjectDTO project) {
        return projectService.add(project);
    }

    @Override
    @DeleteMapping
    public void delete(@RequestBody @Nullable final ProjectDTO project) {
        projectService.remove(project);
    }

    @Override
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") @Nullable final String id) {
        projectService.removeById(id);
    }

    @Override
    @NotNull
    @PutMapping
    public ProjectDTO update(@RequestBody @Nullable final ProjectDTO project) {
        return projectService.update(project);
    }

}

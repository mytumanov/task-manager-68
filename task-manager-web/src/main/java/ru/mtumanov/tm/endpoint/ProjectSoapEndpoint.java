package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.soap.*;

@Endpoint
public class ProjectSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.mtumanov.ru/dto/soap";

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    private ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    private ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    private ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(projectService.findById(request.getId()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    private ProjectCreateRequest save(@RequestPayload final ProjectCreateRequest request) {
        @NotNull final ProjectCreateRequest response = new ProjectCreateRequest();
        response.setProject(projectService.add(request.getProject()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    private ProjectUpdateRequest update(@RequestPayload final ProjectUpdateRequest request) {
        @NotNull final ProjectUpdateRequest response = new ProjectUpdateRequest();
        response.setProject(projectService.update(request.getProject()));
        return response;
    }

}

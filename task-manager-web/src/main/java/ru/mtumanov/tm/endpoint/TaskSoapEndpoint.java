package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.soap.*;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.mtumanov.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    private TaskDeleteResponse delete(@RequestPayload final TaskDeleteRequest request) {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    private TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAll());
        return response;
    }

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    private TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        response.setTask(taskService.findById(request.getId()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    private TaskCreateRequest save(@RequestPayload final TaskCreateRequest request) {
        @NotNull final TaskCreateRequest response = new TaskCreateRequest();
        response.setTask(taskService.add(request.getTask()));
        return response;
    }

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    private TaskUpdateRequest update(@RequestPayload final TaskUpdateRequest request) {
        @NotNull final TaskUpdateRequest response = new TaskUpdateRequest();
        response.setTask(taskService.update(request.getTask()));
        return response;
    }
}

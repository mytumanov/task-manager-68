package ru.mtumanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO add(@Nullable final ProjectDTO project) {
        if (project == null)
            throw new IllegalArgumentException();
        project.setUserId("d885e880-2f7f-4f85-a44f-b365dfc3ba42");
        return projectRepository.save(project);
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO update(@Nullable final ProjectDTO project) {
        if (project == null)
            throw new IllegalArgumentException();
        project.setUserId("d885e880-2f7f-4f85-a44f-b365dfc3ba42");
        return projectRepository.save(project);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable final ProjectDTO project) {
        if (project == null)
            throw new IllegalArgumentException();
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        projectRepository.deleteById(id);
    }

    @Override
    @Nullable
    @Transactional
    public ProjectDTO findById(@Nullable final String id) {
        if (id == null)
            throw new IllegalArgumentException();
        return projectRepository.findById(id).orElse(null);
    }

}

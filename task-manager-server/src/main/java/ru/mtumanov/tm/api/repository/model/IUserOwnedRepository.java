package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.mtumanov.tm.model.AbstractUserOwnedModel;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void deleteByUserId(@NotNull String userId);

    @Nullable
    M findByUserIdAndId(@NotNull String userId, @NotNull String id);

    List<M> findAllByUserId(@NotNull String userId);

    List<M> findAllByUserId(@NotNull String userId, Sort sort);

    long countByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}

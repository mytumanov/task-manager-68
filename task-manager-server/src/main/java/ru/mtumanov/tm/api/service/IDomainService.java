package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.exception.AbstractException;

import java.sql.SQLException;

public interface IDomainService {

    void loadDataBase64() throws AbstractException, SQLException;

    void saveDataBase64() throws AbstractException, SQLException;

    void loadDataBinary() throws AbstractException, SQLException;

    void saveDataBinary() throws AbstractException, SQLException;

    void loadDataJsonFasterXml() throws AbstractException, SQLException;

    void saveDataJsonFasterXml() throws AbstractException, SQLException;

    void loadDataJsonJaxb() throws AbstractException, SQLException;

    void saveDataJsonJaxb() throws AbstractException, SQLException;

    void loadDataXmlFasterXml() throws AbstractException, SQLException;

    void saveDataXmlFasterXml() throws AbstractException, SQLException;

    void loadDataXmlJaxb() throws AbstractException, SQLException;

    void saveDataXmlJaxb() throws AbstractException, SQLException;

    void loadDataYamlFasterXml() throws AbstractException, SQLException;

    void saveDataYamlFasterXml() throws AbstractException, SQLException;

}

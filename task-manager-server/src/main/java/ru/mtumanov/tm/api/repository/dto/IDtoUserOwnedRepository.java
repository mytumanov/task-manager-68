package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

@NoRepositoryBean
public interface IDtoUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    void deleteByUserId(@NotNull String userId);

    @Nullable
    M findByUserIdAndId(@NotNull String userId, @NotNull String id);

    List<M> findAllByUserId(@NotNull String userId);

    List<M> findAllByUserId(@NotNull String userId, Sort sort);

    long countByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}

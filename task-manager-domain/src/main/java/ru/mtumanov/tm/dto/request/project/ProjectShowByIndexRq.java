package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public ProjectShowByIndexRq(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}
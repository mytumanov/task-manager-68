package ru.mtumanov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractRq;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRq extends AbstractRq {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRq(@Nullable final String login, @Nullable final String password) {
        this.login = login;
        this.password = password;
    }

}
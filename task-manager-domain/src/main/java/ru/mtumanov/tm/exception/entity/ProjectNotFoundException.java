package ru.mtumanov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("ERROR! Project not found!");
    }

}
